#
# ****** Conquer Online Tool ******
# Copyright (C) 2013 CptSky
#

#
# enforce out-of-tree build
#
INCLUDE(MacroEnsureOutOfSourceBuild)
MACRO_ENSURE_OUT_OF_SOURCE_BUILD("Build must be out of source !")

#
# build directories
#
SET(BUILD_OUTPUT_DIRECTORY   "${CMAKE_BINARY_DIR}/build" CACHE PATH "Build output directory")
SET(RUNTIME_OUTPUT_DIRECTORY "${BUILD_OUTPUT_DIRECTORY}/bin" CACHE PATH "Runtime output directory")
SET(LIBRARY_OUTPUT_DIRECTORY "${BUILD_OUTPUT_DIRECTORY}/lib" CACHE PATH "Library output directory")
SET(ARCHIVE_OUTPUT_DIRECTORY "${BUILD_OUTPUT_DIRECTORY}/lib" CACHE PATH "Library output directory")
SET(INCLUDE_OUTPUT_DIRECTORY "${BUILD_OUTPUT_DIRECTORY}/include" CACHE PATH "Header output directory")
SET(DOC_OUTPUT_DIRECTORY     "${BUILD_OUTPUT_DIRECTORY}/share/doc" CACHE PATH "Documentation output directory")

SET(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${RUNTIME_OUTPUT_DIRECTORY}")
SET(CMAKE_LIBRARY_OUTPUT_DIRECTORY "${LIBRARY_OUTPUT_DIRECTORY}")
SET(CMAKE_ARCHIVE_OUTPUT_DIRECTORY "${ARCHIVE_OUTPUT_DIRECTORY}")

MESSAGE("Build directory is \"${BUILD_OUTPUT_DIRECTORY}\"")

#
# install directories
#
#SET(CMAKE_INSTALL_PREFIX "${BUILD_OUTPUT_DIRECTORY}")
SET(INSTALL_DIR "${CMAKE_INSTALL_PREFIX}" CACHE PATH "Installation directory")
SET(INSTALL_BIN_DIR "${INSTALL_DIR}/bin" CACHE PATH "Installation directory for executables")
SET(INSTALL_LIB_DIR "${INSTALL_DIR}/lib" CACHE PATH "Installation directory for libraries")
SET(INSTALL_INC_DIR "${INSTALL_DIR}/include" CACHE PATH "Installation directory for headers")
SET(INSTALL_DOC_DIR "${INSTALL_DIR}/share/doc" CACHE PATH "Installation directory for documentation")

MESSAGE("Install directory is \"${INSTALL_DIR}\"")

#
# platform, architecture, build type
#
IF(NOT TARGET_NAME)
  SET(TARGET_NAME "${CMAKE_SYSTEM_NAME}")
  MESSAGE("Found target operating system: ${TARGET_NAME}")
ENDIF(NOT TARGET_NAME)

IF(NOT TARGET_VERSION)
  SET(TARGET_VERSION "${CMAKE_SYSTEM_VERSION}")
  MESSAGE("Found target system version: ${TARGET_VERSION}")
ENDIF(NOT TARGET_VERSION)

IF(NOT TARGET_ARCH)
  SET(TARGET_ARCH "${CMAKE_SYSTEM_PROCESSOR}")
  MESSAGE("Found target architecture: ${TARGET_ARCH}")
ENDIF(NOT TARGET_ARCH)

IF(NOT CMAKE_BUILD_TYPE)
  SET(CMAKE_BUILD_TYPE "Debug")
  MESSAGE("Build type not set ... building for ${CMAKE_BUILD_TYPE}")
ENDIF(NOT CMAKE_BUILD_TYPE)

####################################
# Compiler Settings
####################################

#
# additionnal flags
#
IF(CMAKE_COMPILER_IS_GNUCXX)
  ADD_DEFINITIONS(
    -fPIC -Wall -Wundef -Wpointer-arith
    -Wmissing-format-attribute
    )
ELSEIF(CMAKE_CXX_COMPILER MATCHES "clang")
  ADD_DEFINITIONS(
    -fPIC -Wall
    )
ENDIF(CMAKE_COMPILER_IS_GNUCXX)

#
# include the build directories
#
INCLUDE_DIRECTORIES("${INCLUDE_OUTPUT_DIRECTORY}")
# LINK_DIRECTORIES("${LIBRARY_OUTPUT_DIRECTORY}" "${ARCHIVE_OUTPUT_DIRECTORY}")
