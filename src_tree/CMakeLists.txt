#
# ****** Conquer Online Tool ******
# Copyright (C) 2013 CptSky
#

PROJECT("WDB.MANAGER")
CMAKE_MINIMUM_REQUIRED(VERSION 2.8)

SET(CMAKE_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR}/../cmake)
INCLUDE(common)

INCLUDE_DIRECTORIES(BEFORE
  "include"
)

SET(HEADERS
  "include/cipher.h"
  "include/wdb.h"
)

SET(SOURCES
  "src/cipher.cpp"
  "src/wdb.cpp"
)

ADD_EXECUTABLE("wdb" ${HEADERS} ${SOURCES} "src/program.cpp")
INSTALL_TARGETS("${INSTALL_BIN_DIR}" "wdb")
