/**
 * ****** Conquer Online Tool ******
 * Copyright (C) 2013 CptSky
 *
 * Released in the CO2 Underground for the CO2 Underground.
 */

#ifndef _CO2_TOOL_WDB_H_
#define _CO2_TOOL_WDB_H_

#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <string>
#include <vector>

/**
 * WDB package.
 */
class WDB
{
public:
    #pragma pack(push, 1)
    typedef struct
    {
        /** File format identifier. (BDMG) */
        char Identifier[4];
        /** Total file size in bytes, including the header. */
        int32_t Size;
        /** Offset where the entries start */
        int32_t Offset;
        /** Number of entries inside the package. */
        uint32_t Count;
    }Header; //!< Package header
    #pragma pack(pop)

    #pragma pack(push, 1)
    typedef struct
    {
        /** Hash of the entry ?? Not really required to extract anyway. */
        uint32_t Hash;
        /** Offset where the entry's data start. */
        int32_t Offset;
        /** Total size in bytes of the data. */
        int32_t Size;
        /** Unknown data. Not really required to extract anyway. */
        uint8_t Padding[8]; // unknown
        /** Relative name of the entry (e.g. ini/3dobj.dbc) */
        char Name[64];
    }Entry; //!< Entry information
    #pragma pack(pop)

public:
    /* constructor */
    WDB();
    /* destructor */
    ~WDB();

    /**
     * Open a WDB package.
     *
     * @param[in]    aPath      the path of the package to open for reading
     *
     * @retval TRUE on success
     * @returns FALSE otherwise
     */
    bool open(const char* aPath);

    /**
     * Close the file handle and release all resources.
     */
    void close();

    /**
     * Extract all entries to the specified destination folder.
     *
     * @param[in]     aDestination    the destination folder (without the ending separator)
     *
     * @retval TRUE on success
     * @returns FALSE otherwise
     */
    bool extractAll(const char* aDestination);

private:
    /* Extract one entry. */
    bool extract(const Entry* aEntry, const char* aDestination);

private:
    FILE* mStream; //!< the WDB file stream

    Header mHeader; //!< the WDB's header
    std::vector<Entry*> mEntries; //!< the WDB's entries (info)
};

#endif // _CO2_TOOL_WDB_H_
