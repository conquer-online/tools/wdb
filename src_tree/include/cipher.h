/**
 * ****** Conquer Online Tool ******
 * Copyright (C) 2013 CptSky
 *
 * Released in the CO2 Underground for the CO2 Underground.
 */

#ifndef _CO2_TOOL_CIPHER_H_
#define _CO2_TOOL_CIPHER_H_

#include <stddef.h>
#include <stdint.h>

/**
 * Unknown (probably custom) block cipher used by TQ Digital to
 * encrypt the entry headers.
 */
class Cipher
{
public:
    /**
     * Decrypt a buffer using the custom algorithm.
     *
     * @param[in,out]  aBuf    the buffer to decrypt
     * @param[in]      aLen    the length of the buffer in bytes
     *                         the length must be a multiple of 4
     */
    static void decrypt(uint8_t* aBuf, size_t aLen);

private:
    /** The key size in bytes. */
    static const size_t KEY_SIZE = 256;
    /** The global key. */
    static const uint32_t KEY[KEY_SIZE];
};

#endif // _CO2_TOOL_CIPHER_H_
