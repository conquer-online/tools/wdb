/**
 * ****** Conquer Online Tool ******
 * Copyright (C) 2013 CptSky
 *
 * Released in the CO2 Underground for the CO2 Underground.
 */

#include "wdb.h"
#include "cipher.h"
#include <assert.h>

#ifdef _WIN32
#include <windows.h>
#include <io.h> // _open_osfhandle
#endif // _WIN32

using namespace std;

// If the error is success, execute the function and update the error
#define DOIF(succ, fn)      \
    if (succ)               \
        succ = fn

/////////////////////////////////////////////////////////////
//// I/O extensions (generally outside...)
/////////////////////////////////////////////////////////////

#ifdef _WIN32
static const char PATH_SEPARATOR('\\');
#else
static const char PATH_SEPARATOR('/');
#endif

template<class T>
inline bool
read(T& aOutVal, FILE* aStream) { return (fread(&aOutVal, sizeof(aOutVal), 1, aStream) == 1); }

template bool read<int8_t>(int8_t& aOutVal, FILE* aStream);
template bool read<int16_t>(int16_t& aOutVal, FILE* aStream);
template bool read<int32_t>(int32_t& aOutVal, FILE* aStream);
template bool read<int64_t>(int64_t& aOutVal, FILE* aStream);
template bool read<uint8_t>(uint8_t& aOutVal, FILE* aStream);
template bool read<uint16_t>(uint16_t& aOutVal, FILE* aStream);
template bool read<uint32_t>(uint32_t& aOutVal, FILE* aStream);
template bool read<uint64_t>(uint64_t& aOutVal, FILE* aStream);

bool
read(void* aBuf, size_t aLen, FILE* aStream)
{
    assert(aBuf != NULL);
    assert(aLen > 0);

    return (fread(aBuf, sizeof(uint8_t), aLen, aStream) == aLen);
}

FILE*
getTempFile()
{
    FILE* file = NULL;

    // Windows tries to create the file directly at C:\ root, requiring
    // administrator rights (at least on Windows Vista and later)...
    // GetTempPath & GetTempFileName will gives a valid temp file path
    // to be created with the attribute TEMPORARY...
    // Else, tmpfile() works on UNIX-like
    #if defined(_WIN32)
    wchar_t path[MAX_PATH];
    wchar_t name[MAX_PATH];

    HANDLE hFile = INVALID_HANDLE_VALUE;
    if (GetTempPathW(MAX_PATH, path) != 0)
    {
        if (GetTempFileNameW(path, L"CPT$", 0, name) != 0)
        {
            hFile = CreateFileW(name,
                                GENERIC_READ | GENERIC_WRITE,
                                0,
                                NULL,
                                CREATE_ALWAYS, FILE_ATTRIBUTE_TEMPORARY | FILE_FLAG_DELETE_ON_CLOSE,
                                NULL);
        }
    }

    if (hFile != INVALID_HANDLE_VALUE)
    {
        file = _fdopen(_open_osfhandle((intptr_t)hFile, 0), "rb+");
    }
    #else
    file = tmpfile();
    #endif

    return file;
}

/////////////////////////////////////////////////////////////
//// WDB class
/////////////////////////////////////////////////////////////

WDB :: WDB()
    : mStream(NULL)
{

}

WDB :: ~WDB()
{
    close();
}

bool
WDB :: open(const char* aPath)
{
    assert(aPath != NULL && aPath[0] != '\0');

    close();
    mStream = fopen(aPath, "rb");

    bool success = true;

    if (mStream == NULL)
        success = false;

    DOIF(success, read(mHeader.Identifier, 4, mStream));
    DOIF(success, read(mHeader.Size, mStream));
    DOIF(success, read(mHeader.Offset, mStream));
    DOIF(success, read(mHeader.Count, mStream));

    if (success)
        success = (fseek(mStream, mHeader.Offset, SEEK_SET) == 0);

    if (success)
    {
        FILE* stream = getTempFile();
        if (stream != NULL)
        {
            size_t len = mHeader.Count * sizeof(Entry);
            uint8_t* data = new uint8_t[len];

            DOIF(success, read(data, len, mStream));
            for (uint32_t i = 0; success && i < mHeader.Count; ++i) // each entry is encoded as one block
                Cipher::decrypt(data + (i * sizeof(Entry)), sizeof(Entry));

            // write decrypted data to a temporary file...
            fwrite(data, sizeof(uint8_t), len, stream);
            fflush(stream);
            fseek(stream, 0, SEEK_SET);

            Entry* entry = NULL;
            for (uint32_t i = 0; success && i < mHeader.Count; ++i)
            {
                entry = new Entry();
                DOIF(success, read(entry, sizeof(Entry), stream));

                if (success)
                {
                    mEntries.push_back(entry);
                    entry = NULL;
                }

                delete entry;
            }

            delete[] data;
            fclose(stream);
        }
        else
        {
            fprintf(stderr, "Failed to open a temporary file...\n");
            success = false;
        }
    }

    return success;
}

void
WDB :: close()
{
    if (mStream != NULL)
    {
        fclose(mStream);
        mStream = NULL;
    }

    for (vector<Entry*>::const_iterator
            it = mEntries.begin(), end = mEntries.end();
         it != end; ++it)
    {
        Entry* entry = *it;
        delete entry;
    }
    mEntries.clear();
}

bool
WDB :: extractAll(const char* aDestination)
{
    assert(aDestination != NULL && aDestination[0] != '\0');
    assert(mStream != NULL);

    bool success = true;

    for (vector<Entry*>::const_iterator
            it = mEntries.begin(), end = mEntries.end();
         success && it != end; ++it)
    {
        const Entry* entry = *it;
        success = extract(entry, aDestination);
    }

    return success;
}

bool
WDB :: extract(const Entry* aEntry, const char* aDestination)
{
    assert(aEntry != NULL);
    assert(aDestination != NULL && aDestination[0] != '\0');
    assert(mStream != NULL);

    bool success = true;

    string path(aDestination);
    path += PATH_SEPARATOR;
    path += aEntry->Name;

    FILE* stream = fopen(path.c_str(), "wb");
    if (stream == NULL)
        success = false;

    if (success)
    {
        success = (fseek(mStream, aEntry->Offset, SEEK_SET) == 0);

        uint8_t buffer[4096];
        size_t size = aEntry->Size;
        while (success && size >= sizeof(buffer))
        {
            DOIF(success, read(buffer, sizeof(buffer), mStream));
            fwrite(buffer, sizeof(buffer), 1, stream);

            size -= sizeof(buffer);
        }

        if (success && size > 0)
        {
            DOIF(success, read(buffer, size, mStream));
            fwrite(buffer, size, 1, stream);
        }

        fclose(stream);
    }

    return success;
}
