/**
 * ****** Conquer Online Tool ******
 * Copyright (C) 2013 CptSky
 *
 * Released in the CO2 Underground for the CO2 Underground.
 */

#include <stdlib.h>
#include <stdio.h>
#include "wdb.h"

// If the error is success, execute the function and update the error
#define DOIF(succ, fn)      \
    if (succ)               \
        succ = fn

int main(int argc, char* argv[])
{
    if (argc < 3)
    {
        fprintf(stderr, "Please call the program with arguments (source, destination)\n");
        fprintf(stderr, "wdb /path/to/pkg.wdb /path/to/dest\n");
        return EXIT_FAILURE;
    }

    bool success = true;

    WDB wdb;
    DOIF(success, wdb.open(argv[1]));
    DOIF(success, wdb.extractAll(argv[2]));
    wdb.close();

    return success ? EXIT_SUCCESS : EXIT_FAILURE;
}
