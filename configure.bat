@echo off
title Configure CMake
cls

set BUILD_TYPE=Debug

set OUTPUT_DIR=.\build\%BUILD_TYPE%
echo Creating build directory at: '%OUTPUT_DIR%'
if not exist %OUTPUT_DIR% mkdir %OUTPUT_DIR%
cd %OUTPUT_DIR%

set SOURCE_DIR=..\..\src_tree
echo Looking for CMakeLists.txt in: '%SOURCE_DIR%\CMakeLists.txt'
if not exist %SOURCE_DIR%\CMakeLists.txt (
	echo Cannot find CMakeLists.txt
	exit
)

:: pass the right args to cmake
cmake -DCMAKE_BUILD_TYPE="%BUILD_TYPE%" "%SOURCE_DIR%"